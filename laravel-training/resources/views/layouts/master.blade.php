<!-- This is layout template of each pages display -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- define title customizable -->
    <title>@yield('head.title')</title>

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/app.css">
    <!-- define head of css customizable -->
    @yield('head.css')
</head>
<body>

    @include ('partials.navbar')

    <!-- define body content customizable -->
    @yield('body.content')

    @include ('partials.footer')

    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <!-- define body of js customizable -->
    @yield('body.js')
</body>
</html>