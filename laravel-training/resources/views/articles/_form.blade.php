<div class="form-group">
    {!! Form::label('title', 'Title blog', [ 'class' => 'control-label' ]) !!}
    {!! Form::text('title', null, [ 'id' => 'title', 'class' => 'form-control', 'placeholder' => 'Input title blog here', 'required' => 'true' ]) !!}
</div>

<div class="form-group">
    {!! Form::label('content', 'Blog content', [ 'class' => 'control-label' ]) !!}
    {!! Form::text('content', null, [ 'id' => 'content', 'class' => 'form-control', 'placeholder' => 'Input blog content here', 'required' => 'true' ]) !!}
</div>

<div class="form-group">
    {!! Form::submit($button_name, [ 'class' => 'btn btn-primary' ]) !!}
</div>