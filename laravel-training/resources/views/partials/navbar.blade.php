<nav class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Simple Blog</a>
    </div>

    @if (Auth::check())
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{ route('article.create') }}">New article</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                
                <li><a href="#">Hello, {{ $user->name }}  !</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Menu <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{!! URL::to('auth/logout') !!}">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    @else
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Menu <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{!! URL::to('auth/register') !!}">Register</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    @endif
</nav>