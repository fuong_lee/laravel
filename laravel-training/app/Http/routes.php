<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

// Default page route
Route::get('/', 'PagesController@index');

// Built-in authenticate route
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

// List of articles route
Route::get('/articles', [
    'as' => 'article.index',
    'uses' => 'ArticlesController@index'
]);

// Create new article route
Route::get('/articles/create', [
    'as' => 'article.create',
    'uses' => 'ArticlesController@create'
]);

// Store article data (saved) route
Route::post('/articles', [
    'as' => 'article.store',
    'uses' => 'ArticlesController@store'
]);

// Edit article route
Route::get('/articles/{id}/edit', [
    'as' => 'article.edit',
    'uses' => 'ArticlesController@edit'
]);

// Update article info route
Route::put('/articles/{id}', [
    'as' => 'article.update',
    'uses' => 'ArticlesController@update'
]);

// Delete article route
Route::delete('/articles/{id}', [
    'as' => 'article.destroy',
    'uses' => 'ArticlesController@destroy'
]);

// Show article info route
Route::get('/articles/{id}', [
    'as'   => 'article.show',
    'uses' => 'ArticlesController@show'
]);

