<?php namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article;
use Illuminate\Http\Request;
use Input;
use App\Http\Requests\ArticleFormRequest;

class ArticlesController extends Controller {

    // Make sure that user must authenticated before use any functions in this controller
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $articles = Article::paginate(5);

        return view('articles.index', compact('articles', 'user'));
    }

    public function show($id)
    {
        $user = Auth::user();
        $article = Article::find($id);

        return view('articles.show')
            ->with('user', $user)
            ->with('article', $article);
    }

    public function create()
    {
        $user = Auth::user();

        return view('articles.create')
            ->with('user', $user);
    }

    public function store(ArticleFormRequest $request)
    {
        $title = $request->input('title');
        $content = $request->input('content');

        Article::create([
            'title' => $title,
            'content' => $content
        ]);

        return redirect()->route('article.index');
    }

    public function edit($id)
    {
        $user = Auth::user();
        $article = Article::find($id);

        return view('articles.edit', compact('article', 'user'));
    }

    public function update($id, ArticleFormRequest $request)
    {
        $article = Article::find($id);

        $article->update([
            'title' => $request->get('title'),
            'content' => $request->get('content')
        ]);

        return redirect()->route('article.index');
    }

    public function destroy($id)
    {
        $article = Article::find($id);
        $article->delete();

        return redirect()->route('article.index');
    }
}
